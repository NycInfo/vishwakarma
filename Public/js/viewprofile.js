function viewprofile(uid){
  var data = {};
  data.uid = uid;
  $.ajax({
    type: 'POST',
    data: JSON.stringify(data),
    contentType: 'application/json',
    url: '/getprofiledetailsfromusers',
    async:true,
    crossDomain:true,
      success: function(res) {
        if (res.photo1 == undefined || res.photo1 == "") {
          var img1 = "Public/images/profile_photos/noimage.jpeg"
        }else {
          var img1 = "Public/images/profile_photos/"+res.photo1;
        }
        if (res.photo2 == undefined || res.photo2 == "") {
          var img2 = "Public/images/profile_photos/noimage.jpeg"
        }else {
          var img2 = "Public/images/profile_photos/"+res.photo2;
        }
        if (res.photo3 == undefined || res.photo3 == "") {
          var img3 = "Public/images/profile_photos/noimage.jpeg"
        }else {
          var img3 = "Public/images/profile_photos/"+res.photo3;
        }
        if (res.photo4 == undefined || res.photo4 == "") {
          var img4 = "Public/images/profile_photos/noimage.jpeg"
        }else {
          var img4 = "Public/images/profile_photos/"+res.photo4;
        }
        var viewimages = '<li data-thumb="'+img1+'"><img src="'+img1+'" /></li><li data-thumb="'+img2+'"><img src="'+img2+'" /></li><li data-thumb="'+img3+'"><img src="'+img3+'" /></li><li data-thumb="'+img4+'"><img src="'+img4+'" /></li>';
        $("#viewprofile_images").html(viewimages);
        $('.flexslider').flexslider({
          animation: "slide",
          controlNav: "thumbnails"
        });
        var age = calculateage(res.dob)
        $("#mat_id").html(res.matrimony_id);
        $(".age").html(age);
        $("#mem_email").html(res.mem_email);
        $("#mobile").html(res.mobile);
        $("#created_by").html(res.created_by);
        $(".username").html(res.first_name+' '+res.last_name);
        $("#marital_status").html(res.marital_status);
        $("#sub_cast").html(getsubcaste(res.sub_cast));
        $("#profile_subcaste").html(getsubcaste(res.sub_cast));
        $("#mother_tongue").html(res.mother_tongue);

        $("#profile_name").html(res.first_name+' '+res.last_name);
        $("#profile_mstatus").html(res.marital_status);
        $("#profile_createdfor").html(res.created_by);
        $("#profile_age").html(age);
        $("#profile_mothertogue").html(getmothertongue(res.mother_tongue));
        $("#profile_dob").html(moment(res.dob).format("YYYY-MM-DD"));
      }
  });

function getmothertongue(val){
  if (val == "2") {
    return "Telugu";
  }
  if (val == "1") {
    return "Hindi";
  }
  else {
    return "Others";
  }
}

  $.ajax({
    type: 'POST',
    data: JSON.stringify(data),
    contentType: 'application/json',
    url: '/getprofiledetailsfromprofile',
    async:true,
    crossDomain:true,
      success: function(res) {

        $("#views").html(res.views)
        $("#Height").html(res.mem_height);
        $("#profile_tob").html(res.mem_tob);
        $("#profile_pob").html(res.mem_pob);
        console.log(res.mem_raasi);
        $("#profile_raasi").html(getrasi(res.mem_raasi));
        $("#profile_star").html(getstar(res.mem_janmanakshatra));
        $("#profile_padam").html(res.mem_padam);
        $("#profile_gothram").html(res.mem_gothram);
        $("#mem_complexion").html(res.mem_complexion);
        $("#mem_weight").html(res.mem_weight);

        //Education Details
        if(isNaN(res.mem_education)){
          var education = res.mem_education;
         }else{
           var education = geteducation(res.mem_education);
         }

        $("#mem_education").html(education);
        $("#memedu_splizn").html(res.memedu_splizn);
        $("#mem_education2").html(res.mem_education2);
        $("#memedu_splizn2").html(res.memedu_splizn2);
        $("#mem_occupation").html(res.mem_occupation);
        $("#mem_working").html(res.mem_working);
        $("#mem_salary").html(res.mem_salary);
        //Fmily Details
        $("#father_name").html(res.father_name);
        $("#father_occuption").html(res.father_occuption);
        $("#mother_name").html(res.mother_name);
        $("#mother_occuption").html(res.mother_occuption);
        $("#guardian_name").html(res.guardian_name);
        $("#guardian_occuption").html(res.guardian_occuption);
        $("#brothers").html(res.brothers);
        $("#married_brothers").html(res.married_brothers);
        $("#sisters").html(res.sisters);
        $("#married_sisters").html(res.married_sisters);



        $("#mem_ancestralorigin").html(res.mem_ancestralorigin);
        $("#mem_presentsettledin").html(res.mem_presentsettledin);

        $("#mem_kujadosham").html(res.mem_kujadosham);

        $("#partner_preferences").html(res.partner_preferences_Words);
        $("#address").html(res.address);
        $("#alternate_email").html(res.alternate_email);
        $("#alternate_mobile").html(res.alternate_mobile);
        $("#family_details").html(res.family_details);
        $("#property_details").html(res.property_details);

        $("#profile_complexian").html(res.mem_complexion);
        $("#profile_height").html(res.mem_height);

        $("#address").html(res.address);

      }
  });
}


function myprofile(uid,source){
  var data = {};
  data.uid = uid;
  data.source=source;
  $.ajax({
    type: 'POST',
    data: JSON.stringify(data),
    contentType: 'application/json',
    url: '/getmyprofiledetailsfromusers',
    async:true,
    crossDomain:true,
      success: function(res) {
        if (res.photo1 == undefined || res.photo1 == "") {
          var img1 = "Public/images/profile_photos/noimage.jpeg"
        }else {
          var img1 = "Public/images/profile_photos/"+res.photo1;
        }
        if (res.photo2 == undefined || res.photo2 == "") {
          var img2 = "Public/images/profile_photos/noimage.jpeg"
        }else {
          var img2 = "Public/images/profile_photos/"+res.photo2;
        }
        if (res.photo3 == undefined || res.photo3 == "") {
          var img3 = "Public/images/profile_photos/noimage.jpeg"
        }else {
          var img3 = "Public/images/profile_photos/"+res.photo3;
        }
        if (res.photo4 == undefined || res.photo4 == "") {
          var img4 = "Public/images/profile_photos/noimage.jpeg"
        }else {
          var img4 = "Public/images/profile_photos/"+res.photo4;
        }
        var viewimages = '<li data-thumb="'+img1+'"><img src="'+img1+'" /></li><li data-thumb="'+img2+'"><img src="'+img2+'" /></li><li data-thumb="'+img3+'"><img src="'+img3+'" /></li><li data-thumb="'+img4+'"><img src="'+img4+'" /></li>';
        $("#myprofile_images").html(viewimages);
        $('.flexslider').flexslider({
          animation: "slide",
          controlNav: "thumbnails"
        });
        var image1 = '<img src="'+img1+'"  class="img-responsive" width="100%" alt="">';
        var image2 = '<img src="'+img2+'"  class="img-responsive" width="100%" alt="">';
        var image3 = '<img src="'+img3+'"  class="img-responsive" width="100%" alt="">';
        var image4 = '<img src="'+img4+'"  class="img-responsive" width="100%" alt="">';
        $("#photo1").html(image1);
        $("#photo2").html(image2);
        $("#photo3").html(image3);
        $("#photo4").html(image4);
        var age = calculateage(res.dob)
        $("#mat_id").html(res.matrimony_id);
        //$("#age").html(age);
        $(".age").html(age);
        $("#mem_email").html(res.mem_email);
        $("#mobile").html(res.mobile);
        $("#created_by").html(res.created_by);
        $("#profilefor").val(res.created_by);

        $(".username").html(res.first_name+' '+res.last_name);
        $("#marital_status").html(res.marital_status);
        $("#sub_cast").html(getsubcaste(res.sub_cast));
        $("#profile_subcaste").html(getsubcaste(res.sub_cast));
        $("#mother_tongue").html(res.mother_tongue);

        $("#profile_name").html(res.first_name+' '+res.last_name);
        $("#profile_mstatus").html(res.marital_status);
        $("#profile_createdfor").html(res.created_by);
        $("#profile_age").html(age);
        $("#profile_mothertogue").html(getmothertongue(res.mother_tongue));
        $("#profile_dob").html(moment(res.dob).format("YYYY-MM-DD"));

        function getmothertongue(val){
          if (val == "2") {
            return "Telugu";
          }
          if (val == "1") {
            return "Hindi";
          }
          else {
            return "Others";
          }
        }
        $("#firstname").val(res.first_name);
        $("#lastname").val(res.last_name);
        $("#gender").val(res.gender);
        $("#email").val(res.mem_email);
        $("#password").val(res.mem_pass);
        $("#mobilenumber").val(res.mobile);
        $(".mobilenumbermm").val(res.mobile);
        var date = moment(res.dob).format("DD")
        var month = moment(res.dob).format("MM")
        var year = moment(res.dob).format("YYYY")
        $(".dob_date").val(date);
        $(".dob_month").val(month);
        $(".dob_year").val(year);
        $("#maritalstatus").val(res.marital_status);
        $("#subcaste").val(res.sub_cast);
        $("#mothertongue").val(res.mother_tongue);
        $("#country").val(res.country);

      }
  });



  $.ajax({
    type: 'POST',
    data: JSON.stringify(data),
    contentType: 'application/json',
    url: '/getmyprofiledetailsfromprofile',
    async:true,
    crossDomain:true,
      success: function(res) {
        $("#Height").html(res.mem_height);
        $("#profile_tob").html(res.mem_tob);
        $("#profile_pob").html(res.mem_pob);
        $("#profile_raasi").html(res.mem_janmanakshatra);
        $("#profile_gothram").html(res.mem_gothram);
        $("#mem_complexion").html(res.mem_complexion);
        $("#mem_weight").html(res.mem_weight);

        //Education Details
        if(isNaN(res.mem_education)){
          var education = res.mem_education;
         }else{
           var education = geteducation(res.mem_education);
         }
        $("#mem_education").html(education);
        $("#memedu_splizn").html(res.memedu_splizn);
        $("#mem_education2").html(res.mem_education2);
        $("#memedu_splizn2").html(res.memedu_splizn2);
        $("#mem_occupation").html(res.mem_occupation);
        $("#mem_working").html(res.mem_working);
        $("#mem_salary").html(res.mem_salary);
        //Fmily Details
        $("#father_name").html(res.father_name);
        $("#father_occuption").html(res.father_occuption);
        $("#mother_name").html(res.mother_name);
        $("#mother_occuption").html(res.mother_occuption);
        $("#guardian_name").html(res.guardian_name);
        $("#guardian_occuption").html(res.guardian_occuption);
        $("#brothers").html(res.brothers);
        $("#married_brothers").html(res.married_brothers);
        $("#sisters").html(res.sisters);
        $("#married_sisters").html(res.married_sisters);



        $("#mem_ancestralorigin").html(res.mem_ancestralorigin);
        $("#mem_presentsettledin").html(res.mem_presentsettledin);

        $("#mem_kujadosham").html(res.mem_kujadosham);

        $("#partner_preferences").html(res.partner_preferences);
        $("#address").html(res.address);
        $("#alternate_email").html(res.alternate_email);
        $("#alternate_mobile").html(res.alternate_mobile);
        $("#family_details").html(res.family_details);
        $("#property_details").html(res.property_details);

        $("#profile_complexian").html(res.mem_complexion);
        $("#profile_height").html(res.mem_height);

        $("#timeofbirth").val(res.mem_tob);
        $("#star").val(res.mem_janmanakshatra);
        $("#padam").val(res.mem_padam);

        $("#rasi").val(res.mem_raasi);
        $("#profile_raasi").html(getrasi(res.mem_raasi))
        $("#profile_star").html(getstar(res.mem_janmanakshatra));
        $("#profile_padam").html(res.mem_padam);
        $("#gothram").val(res.mem_gothram);

        $("#complexian").val(res.mem_complexion);
        $("#height").val(res.mem_height);

        $("#weight").val(res.mem_weight);
        $("#Education").val(res.mem_education);
        $("#Specialiation").val(res.memedu_splizn);
        $("#Education1").val(res.mem_education2);
        $("#Specialiation1").val(res.memedu_splizn2);

          $("#Occupation").val(res.mem_occupation);
          $("#Working").val(res.mem_working);
          $("#Salary").val(res.mem_salary);
          $("#kujadosam").val(res.mem_kujadosham);
          $("#fathername").val(res.father_name);
          $("#fatheroccupation").val(res.father_occuption)
          $("#mothername").val(res.mother_name);
          $("#motheroccupation").val(res.mother_occuption);
          $("#gaurdianname").val(res.guardian_name);
          $("#gaurdianoccupation").val(res.guardian_occuption);
          $("#brothers_reg").val(res.brothers);
          $("#sisters_reg").val(res.sisters);
          $("#marriedbrothers").val(res.married_brothers);
          $("#marriedsisters").val(res.married_sisters);
          $("#Adresss").val(res.address);
          $("#Village").val(res.Village);
          $("#City").val(res.City);
          $("#State").val(res.State);
          $("#Country").val(res.Country);
          $("#Reference").val(res.references);
          $("#alternateemail").val(res.alternate_email);
          $("#alternatemobile").val(res.alternate_mobile);
          $("#familydetails").val(res.family_details);
          $("#propertydetails").val(res.property_details);
          $("#partnerpreference").val(res.partner_preferences_Words);
      }
  });
}

function getsimilarprofiles(){
  var data = {};
  data.page = 1;
  $.ajax({
    type: 'POST',
    data: JSON.stringify(data),
    contentType: 'application/json',
    url: '/getsimilarprofiles',
    async:true,
    crossDomain:true,
      success: function(res) {
        var msg = '';
        if(res.length>0){
          $.each(res,function(i,d){
            if (d.photo1 == undefined || d.photo1 == "") {
              var img1 = "Public/images/profile_photos/noimage.jpeg"
            }else {
              var img1 = "Public/images/profile_photos/"+d.photo1;
            }
            msg += '<ul class="profile_item"><a href="/viewprofile?uid='+d.uid+'"><li class="profile_item-img"><img src="'+img1+'" class="img-responsive" alt=""/></li><li class="profile_item-desc"><h4>'+d.matrimony_id+'</h4><p>'+d.first_name+' '+d.last_name+'</p><h5><a href="/viewprofile?uid='+d.uid+'">View Full Profile</a></h5></li><div class="clearfix"> </div></a></ul>';
          })
          $("#similiarprofiles").html(msg);
        }else{
          msg += "No Records Found.."
        }
      }
  });
}






// profile_bodytype
// profile_physicalstatus
// profile_drink
// profile_weight
// profile_diet
// profile_smoke


function calculateage(dob){
  var dateTime = moment(dob).format("YYYY-MM-DD");
  var years = moment().diff(dateTime, 'years',false);
  return years;
}
function getsubcaste(caste){
  var data = {};
  data.dtt = caste;
  var msg = '';
  $.ajax({
    type: 'POST',
    data: JSON.stringify(data),
    contentType: 'application/json',
    url: '/getsubcastewithid',
    async:false,
    crossDomain:true,
      success: function(res) {
        msg += res.sc_name;
      }
  });
  return msg;
}
function getstar(val){
  var data = {};
  data.star = val;
  var msg = '';
  $.ajax({
    type: 'POST',
    data: JSON.stringify(data),
    contentType: 'application/json',
    url: '/getstartwithid',
    async:false,
    crossDomain:true,
      success: function(res) {
        msg += res.s_birth_star;
      }
  });
  return msg;
}

function getrasi(val){
  var data = {};
  data.rasi = val;
  console.log(data);
  var msg = '';
  $.ajax({
    type: 'POST',
    data: JSON.stringify(data),
    contentType: 'application/json',
    url: '/getrasitwithid',
    async:false,
    crossDomain:true,
      success: function(res) {
        console.log(res);
        msg += res.s_raasi;
      }
  });
  return msg;
}
function geteducation(val){
  var data = {};
  data.educ = val;
  console.log(data);
  var msg = '';
  $.ajax({
    type: 'POST',
    data: JSON.stringify(data),
    contentType: 'application/json',
    url: '/geteducationtwithid',
    async:false,
    crossDomain:true,
      success: function(res) {
        console.log(res);
        msg += res.education_name;
      }
  });
  return msg;
}
