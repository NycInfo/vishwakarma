var http = require('http');
var fs = require('fs');
var url = require('url');
var path = require('path');
var express = require('express');
var app = express();
var session = require('express-session');
var hart = require('./hart');
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//
// var filePath = 'Public/images/Profiles/1.jpg';
// fs.unlinkSync(filePath);
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');


var response = require('./routes/response');
var responsepayu = require('./routes/responsepayu');

// app.use('/', index);

app.post('/response', response);
app.post('/responsepayu', responsepayu);



require('./routes/mainroute')(app);
require('./routes/RegistrationRoutes')(app);
require('./routes/ResultPageRoutes')(app);
require('./routes/SearchRoutes')(app);
require('./routes/bgroutes')(app);
require('./routes/requestsRoutes')(app);
require('./routes/getprofileRoutes')(app);
require('./routes/AdminRoutes')(app);

hart.mongo.Promise = global.Promise;
var mongoclient = { useMongoClient: true };
hart.mongo.connect(hart.url,mongoclient,function(err, db) {
    if(!err) {
        console.log("mongodb connected successfully...");
    }
});


app.use(express.static(path.join(__dirname, '')));

app.use(session({
secret: 'Niharinfo',
resave: true,
saveUninitialized: true
}));

app.get('/', function(req,res){
  //req.session.destroy();
  delete req.session.Niharinfo;
  res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
  res.sendFile(path.resolve(__dirname, 'Public/index.html'));
});
// app.get('/logout', function(req,res){
//   //req.session.destroy();
//   delete req.session.Niharinfo;
//   res.sendFile(path.resolve(__dirname, 'Public/login.html'));
// });
// Listen for requests
app.listen(hart.port,function(e){
   if(!e)
   {
      console.log('Server running at localhost:'+hart.port);
   }
})
