var express = require('express');
var checksum = require('../lib/checksum');
var router = express.Router();
var hart= require('../hart');
var moment = require('moment');
//var session = require('express-session');
var mongoose = require('mongoose');
var mongoXlsx = require('mongo-xlsx');
var MongoClient = require('mongodb').MongoClient;
var url = hart.url;
var request = require('request');
var qs = require("querystring");  //For Otp service
var http = require("http");     //For Otp service
var sha512 = require('sha512');
var btoa = require('btoa');
var ObjectId = require('mongodb').ObjectID;
var execPhp = require('exec-php');
var sha512 = require('js-sha512');
var querystring = require('querystring');


router.post('/responsepayu', function(req,res,next) {
    if(req.body.status == "success"){
      var sessionid = req.body.productinfo;
      var amount1 = parseFloat(req.body.amount);
      var txnid = req.body.txnid;
      var status = "success";
      var payfrom = "Payubiz"
      var mihpayid = req.body.mihpayid;
      var wallets = hart.wallets;
      var amount2 = amount1/100;
      if(req.body.mode == "CC"){
        var amount3 = amount1 - amount2*1.9;
      }else if (req.body.mode == "DC") {
        if(amount1>2000){
          var perc = 1;
        }else{
          var perc = 0.1;
        }
        var amount3 = amount1 - amount2*perc;
      }else if (req.body.mode == "NB") {
        var amount3 = amount1 - amount2*1.85;
      }else{
        var amount3 = amount1;
      }
      wallets.findOne({ agentId : sessionid },function(err,data){
        if(!err){
            var TotalAMount = data.Wallet + amount3;
            wallets.updateOne({ 'agentId':sessionid },{ $set:{ Wallet: TotalAMount }}, function(err, response) {
              if(!err){
                console.log(data);
                console.log("wallet updated");
              posttozaakpaymentGetWay(sessionid,amount1,amount3,txnid,"success",payfrom,mihpayid);
              }else{
                console.log("Wallet Not Updated");
              }
            });

        }else{
          return err;
        }
       });

    }else{
      posttozaakpaymentGetWay(sessionid,amount1,amount3,txnid,"fail",payfrom,mihpayid);
    }
    var data = {};
    data.TransactionId = req.body.txnid;
    data.Amount = amount3;
    data.Mode = req.body.mode;
    data.Status = req.body.status;
    res.render('responsepayu', {
        data: data
    });
    res.end();
});



function posttozaakpaymentGetWay(agentId,Enteredamount,Amount,TransactionID,status,Paymentfrom,mihpayid){
    var ADDm_Reports = hart.add_money_reports;
        var Ereports = new ADDm_Reports({
          agentId:agentId,
          Enteredamount:Enteredamount,
          Amount: Amount,
          TransactionID:TransactionID,
          Paymentfrom:Paymentfrom,
          mihpayid:mihpayid,
          status:status,
        });
        Ereports.save(function (err, results) {
          if(err){
            console.log(err);
          }else{
            console.log("Add money Status updated");
          }
        });
}

module.exports = router;
