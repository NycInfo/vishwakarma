var async = require('async');
var hart= require('../hart');
var moment = require('moment');
var MongoClient = require('mongodb').MongoClient;
var url = hart.url;
var session = require('express-session');
var mongoose = require('mongoose');
var multer  = require('multer');
var nodemailer = require('nodemailer');
var fs = require('fs');
var ObjectId = require('mongodb').ObjectID;
var nodemailer = require('nodemailer');
var request = require('request');
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, hart.cmsImgPath)
    },
    filename: function (req, file, cb) {
        cb(null, Date.now()+'-'+file.originalname)
    }
})

var upload = multer({ storage: storage });

module.exports = function(app){

  app.use(session({
    secret: 'Niharinfo',
    resave: true,
    saveUninitialized: true
  }));



  app.post('/updatepassword',function(req,res){
      var id = ObjectId(req.session.UserId);
      var collection = hart.users;
      collection.updateOne({ '_id': id },{ $set:{ Password: req.body.newpassword }}, function(err, response) {
      if(err){
        console.log(err);
        res.json(0);
      }
      else{
        res.json(1);
      }
      });
  });

app.post('/Registration',function(req,res){
  var users = hart.matrimony_users;
  var Profiles = hart.matrimony_profile;
  var regcount = hart.regcount;
  regcount.findOne({ Status : "1" },function(err,data){
    if(err){
      console.log(err);
    }
    else{
      var regnum = data.registernumber+1;
      if (req.body.gender == "MALE") {
        var regmatid = "VKMG"+regnum;
      }else {
        var regmatid = "VKMB"+regnum;
      }

      var num = data.NoOfProfilesCreated+1;
      regcount.updateOne({ 'Status': "1" },{ $set:{ registernumber: regnum,matrimonynumber: regmatid }}, function(err, response) {
      if(err){
        console.log(err);
      }
      else{
        var Uid = regnum;
        var mat_id = regmatid;
        var dateofb = req.body.dob_year+"-"+req.body.dob_month+"-"+req.body.dob_date+" 00:00:00.000";
        var years = moment().diff(req.body.dob_year+"-"+req.body.dob_month+"-"+req.body.dob_date, 'years');
          var prousers = new users({
            uid: Uid,
            matrimony_id:mat_id,
            mem_email:req.body.email,
            mem_pass:req.body.password,
            mobile:req.body.mobilenumber,
            created_by:req.body.profilefor,
            first_name:req.body.firstname,
            last_name:req.body.lastname,
            gender:req.body.gender,
            dob:dateofb,
            Age:years,
            marital_status:req.body.maritalstatus,
            sub_cast:req.body.subcaste,
            mother_tongue:req.body.mothertongue,
            food_type:req.body.foodtype,
            country:req.body.country,
            mem_status:"1",
            inactive_reason:"",
            Status:"1",
            rajorpayidPayment:"",
            PaymentStatus:"0",
            Membershipplan:"",
            PaymentDate:"",
            photo1:"",
            photo2:"",
            photo3:"",
            photo4:"",
            photo5: "",
          });
          prousers.save(function (err, results) {
            if(err){
              console.log(err);
              res.json(0);
            }
            else{
              var profiless = new Profiles({
                uid: Uid,
                matrimony_id:mat_id,
                mem_email:req.body.email,
                mem_tob:req.body.timeofbirth,
                mem_pob:req.body.placeofbirth,
                mem_janmanakshatra:req.body.star,
                mem_padam:req.body.padam,
                mem_raasi:req.body.rasi,
                mem_gothram:req.body.gothram,
                mem_complexion:req.body.complexian,
                mem_height:req.body.height,
                mem_weight:req.body.weight,
                mem_education:req.body.Education,
                memedu_splizn:req.body.Specialiation,
                mem_education2:req.body.Education1,
                memedu_splizn2:req.body.Specialiation1,
                mem_occupation:req.body.Occupation,
                mem_working:req.body.Working,
                mem_salary:req.body.Salary,
                mem_ancestralorigin:req.body.ancestralorigin,
                mem_presentsettledin:req.body.City,
                mem_kujadosham:req.body.kujadosam,
                father_name:req.body.fathername,
                father_occuption:req.body.fatheroccupation,
                mother_name:req.body.mothername,
                mother_occuption:req.body.motheroccupation,
                guardian_occuption:req.body.gaurdianname,
                guardian_name:req.body.gaurdianoccupation,
                brothers:req.body.brothers,
                married_brothers:req.body.marriedbrothers,
                sisters:req.body.sisters,
                married_sisters:req.body.marriedsisters,
                partner_preferences_Words:req.body.partnerpreference,
                partner_preferences_SubCaste:"",
                partner_preferences_Specialiation:"",
                partner_preferences_Education:"",
                partner_preferences_State:"",
                partner_preferences_City:"",
                partner_preferences_Height:"",
                address:req.body.Address,
                Village:req.body.Village,
                City:req.body.City,
                State:req.body.State,
                Country:req.body.Country,
                references:req.body.Reference,
                alternate_email:req.body.alternateemail,
                alternate_mobile:req.body.alternatemobile,
                views:0,
                family_details:req.body.familydetails,
                property_details:req.body.partnerpreference,
              });
              profiless.save(function (err, results) {
                if(err){
                  console.log(err);
                  res.json(0);
                }
                else{
                  res.json(1);
                  //sendmail(req.body.email,"Welcome To VSKM","Hi welcome");
                  sendsms(req.body.mobilenumber,"Dear "+req.body.firstname+", Thank you for registering with VKFST. Your Matrimony ID is "+mat_id+" and password is "+req.body.password+". Wishing you luck in your partner search - login :  http://vishwakarmaalliance.com/");
                }
              });
            }
          });
      }
      });
    }
   });
  });

  app.post('/EditProfile',function(req,res){
      if (req.body.source == "admin") {
        var id  = req.body.uid;
      }else{
        var id = req.session.uid;
      }
      var dateofb = req.body.dob_year+"-"+req.body.dob_month+"-"+req.body.dob_date+" 00:00:00.000"
      hart.matrimony_users.updateOne({ 'uid': id },{ $set:{
      mem_email:req.body.email,
      mem_pass:req.body.password,
      mobile:req.body.mobilenumber,
      created_by:req.body.profilefor,
      first_name:req.body.firstname,
      last_name:req.body.lastname,
      gender:req.body.gender,
      dob:dateofb,
      marital_status:req.body.maritalstatus,
      sub_cast:req.body.subcaste,
      mother_tongue:req.body.mothertongue,
      food_type:req.body.foodtype,
      country:req.body.country,
     }}, function(err, response) {
      if(err){
        console.log(err);
      }
      else{
        hart.matrimony_profile.updateOne({ 'uid': id},{ $set:{ mem_email:req.body.email,
        mem_tob:req.body.timeofbirth,
        mem_pob:req.body.City,
        mem_janmanakshatra:req.body.star,
        mem_padam:req.body.padam,
        mem_raasi:req.body.rasi,
        mem_gothram:req.body.gothram,
        mem_complexion:req.body.complexian,
        mem_height:req.body.height,
        mem_weight:req.body.weight,
        mem_education:req.body.Education,
        memedu_splizn:req.body.Specialiation,
        mem_education2:req.body.Education1,
        memedu_splizn2:req.body.Specialiation1,
        mem_occupation:req.body.Occupation,
        mem_working:req.body.Working,
        mem_salary:req.body.Salary,
        mem_ancestralorigin:"",
        mem_presentsettledin:req.body.City,
        mem_kujadosham:req.body.kujadosam,
        father_name:req.body.fathername,
        father_occuption:req.body.fatheroccupation,
        mother_name:req.body.mothername,
        mother_occuption:req.body.motheroccupation,
        guardian_occuption:req.body.gaurdianname,
        guardian_name:req.body.gaurdianoccupation,
        brothers:req.body.brothers,
        married_brothers:req.body.marriedbrothers,
        sisters:req.body.sisters,
        married_sisters:req.body.marriedsisters,
        partner_preferences_Words:req.body.partnerpreference,
        partner_preferences_SubCaste:"",
        partner_preferences_Specialiation:"",
        partner_preferences_Education:"",
        partner_preferences_State:"",
        partner_preferences_City:"",
        partner_preferences_Height:"",
        address:req.body.Address,
        Village:req.body.Village,
        City:req.body.City,
        State:req.body.State,
        Country:req.body.Country,
        references:req.body.Reference,
        alternate_email:req.body.alternateemail,
        alternate_mobile:req.body.alternatemobile,
        views:0,
        family_details:req.body.familydetails,
        property_details:req.body.partnerpreference, }}, function(err, response) {
        if(err){
          console.log(err);
        }
        else{
        res.json(1);
        }
        });
      }
      });
  })

 function randfunc(number){
    var chars = number;
    var string_length = 6;
    var randomstring = '';
    var charCount = 0;
    var numCount = 0;
    for (var i=0; i<string_length; i++) {
        if((Math.floor(Math.random() * 2) == 0) && numCount < 3 || charCount >= 5) {
            var rnum = Math.floor(Math.random() * 10);
            randomstring += rnum;
            numCount += 1;
        } else {
            var rnum = Math.floor(Math.random() * chars.length);
            randomstring += chars.substring(rnum,rnum+1);
            charCount += 1;
        }
    }
    return randomstring;
  }

function sendmail(tomail,subject,msg){
  var transporter = nodemailer.createTransport({
    host: 'dedrelay.secureserver.net',
    port: 25,
    secure: true,
    service: 'gmail',
  //service: 'Godaddy',
    auth: {
    user: 'steaven5953@gmail.com',
    pass: 'nani5953'
    }
  });
  var msg   = "Welcome to Vishwakarma alliances";
  var mailOptions = {
    from: 'steaven5953@gmail.com',
    to: tomail,
    subject: subject,
    html:msg
  };
  transporter.sendMail(mailOptions, function(error, info){
    if (error) {
      console.log(error);
    } else {
      console.log("mail sent");
    }
  });
}

function sendsms(number,message){
  var txt = message;
      var options = { method: 'GET',
      url: 'https://login.spearuc.com/MOBILE_APPS_API/sms_api.php',
      qs:
       { type: 'smsquicksend',
         user: 'nycinfo',
         pass: 'Xun6ah9F',
         sender: 'NYCINF',
         to_mobileno: number,
         sms_text: txt },
      headers:
       {
         'cache-control': 'no-cache' } };
      request(options, function (error, response, body) {
        if (error) throw new Error(error);
        console.log(body);
      });
}

}
