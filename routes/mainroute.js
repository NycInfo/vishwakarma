var path = require('path');
var session = require('express-session');
var hart= require('../hart');
var async = require('async');
var crypto = require('crypto');
var hart= require('../hart');
var url = require('url');
var session = require('express-session');
var mongoose = require('mongoose');
var mongoXlsx = require('mongo-xlsx');
var MongoClient = require('mongodb').MongoClient;
var url = hart.url;
var globals = require('node-global-storage');
var request = require('request');
var ObjectId = require('mongodb').ObjectID;

  var auth = function(req,res,next){
    if(req.session.UserId != "" && req.session.user)
    return next();
    else
    return res.redirect('/');
  };
  var auth1 = function(req,res,next){
    if(req.session.AdminId != ""&&req.session.Admin)
    return next();
    else
    return res.redirect('/Admin');
  };
module.exports = function(app){

  app.use(session({
  secret: 'vshwkrm',
  resave: true,
  saveUninitialized: true
  }));


  app.post('/checkingemailisregisteredalready',function(req,res){
      var collection3 = hart.matrimony_users;
      hart.matrimony_users.count({ "$and": [{"mem_status":"1"},{ "$or": [{"mem_email":req.body.emailentered}, {"mobile": req.body.phoneentered}] }] }).exec(function(err,doc){
        if(!err){
          res.json(doc);
        }else{
          res.json("0");
        }
      });
})
  app.post('/Profilelogin',function(req,res){
    hart.matrimony_users.find({ "$and": [{"mem_pass":req.body.password},{"mem_status":"1"},{ "$or": [{"mobile": req.body.username},{"matrimony_id": req.body.username},{"mem_email": req.body.username}] }] }).exec(function(err,doc){
      if(!err){
        if (doc.length>0) {
          req.session.UserId = doc[0]._id;
          req.session.uid = doc[0].uid;
          req.session.username = doc[0].first_name;
          req.session.matrimony_id = doc[0].matrimony_id;
          req.session.mem_email = doc[0].mem_email;
          req.session.gender = doc[0].gender;
          req.session.mobile = doc[0].mobile;
          req.session.user = true;
          res.json("1")
        }else{
          res.json("0");
        }
      }else{
        res.json("0");
      }
    });
  })

  app.post('/getsessioninfo',function(req,res){
      var id = ObjectId(req.session.UserId);
      var Query = {"_id": id};
      hart.matrimony_users.findOne(Query,function(err,data){
        if(err){
          console.log(err);
        }
        else{
      //    console.log(data);
          res.json(data);
        }
       });
  })

  app.post('/adminlogin',function(req,res){
    var collection2 = hart.admins;
    collection2.find({"username": req.body.userid}).exec(function(err,doc1){
      if (doc1 == "") {
        res.end(JSON.stringify(0));
      }else{
        if(doc1[0].password == req.body.password)
        {
          req.session.AdminId = doc1[0]._id;
          req.session.AdminName = doc1[0].username;
          req.session.Admin = true;
          res.end(JSON.stringify(1));
        }
        else{
          res.end(JSON.stringify(0));
        }
      }
      });
    });

   app.get('/logout', function (req, res) {
    delete req.session.user;
    req.session.destroy();
    res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    res.sendFile(path.resolve(__dirname,'../Public/index.html'));
   });
  app.get('/Index', function (req, res) {
   res.sendFile(path.resolve(__dirname,'../Public/index.html'));
  });
  app.get('/Login', function (req, res) {
   res.sendFile(path.resolve(__dirname,'../Public/login.html'));
  });
  app.get('/forgotpassword', function (req, res) {
   res.sendFile(path.resolve(__dirname,'../Public/forgotpassword.html'));
  });
  app.get('/Registration', function (req, res) {
   res.sendFile(path.resolve(__dirname,'../Public/register.html'));
  });
  app.get('/home',auth,function (req, res) {
   res.sendFile(path.resolve(__dirname,'../Public/home.html'));
  });
  app.get('/My_Profile',auth, function (req, res) {
   res.sendFile(path.resolve(__dirname,'../Public/my_profile.html'));
  });
  app.get('/Contact_us', function (req, res) {
   res.sendFile(path.resolve(__dirname,'../Public/contact.html'));
  });
  app.get('/About_us', function (req, res) {
   res.sendFile(path.resolve(__dirname,'../Public/about.html'));
  });
  app.get('/New_Profiles',auth, function (req, res) {
   res.sendFile(path.resolve(__dirname,'../Public/matches.html'));
  });
  app.get('/upgrade',auth, function (req, res) {
   res.sendFile(path.resolve(__dirname,'../Public/upgrade.html'));
  });
  app.get('/viewprofile',auth, function (req, res) {
   res.sendFile(path.resolve(__dirname,'../Public/view_profile.html'));
  });
  app.get('/edit_profile',auth, function (req, res) {
   res.sendFile(path.resolve(__dirname,'../Public/editprofile.html'));
  });
  app.get('/managephotos',auth, function (req, res) {
   res.sendFile(path.resolve(__dirname,'../Public/managephotos.html'));
  });
  app.get('/Services', function (req, res) {
   res.sendFile(path.resolve(__dirname,'../Public/services.html'));
  });
  app.get('/presidentmsg', function (req, res) {
   res.sendFile(path.resolve(__dirname,'../Public/presidentmessage.html'));
  });
  app.get('/SearchResult',auth, function (req, res) {
   res.sendFile(path.resolve(__dirname,'../Public/searchResults.html'));
  });

  app.get('/Admin', function (req, res) {
   res.sendFile(path.resolve(__dirname,'../Public2/login.html'));
  });
  app.get('/AdminDashboard',auth1,function(req,res){
  res.sendFile(path.resolve(__dirname,'../Public2/index.html'));
  });
  app.get('/AllProfiles',auth1,function(req,res){
    res.sendFile(path.resolve(__dirname,'../Public2/all-profiles.html'));
  });
  app.get('/AllGrooms',auth1,function(req,res){
    res.sendFile(path.resolve(__dirname,'../Public2/allgrooms.html'));
  });
  app.get('/AllBrides',auth1,function(req,res){
    res.sendFile(path.resolve(__dirname,'../Public2/allbrides.html'));
  });
  app.get('/DeletedProfiles',auth1,function(req,res){
    res.sendFile(path.resolve(__dirname,'../Public2/deletedprofiles.html'));
  });
  app.get('/PaidProfiles',auth1,function(req,res){
    res.sendFile(path.resolve(__dirname,'../Public2/paidusers.html'));
  });
  app.get('/UnpaidProfiles',auth1,function(req,res){
    res.sendFile(path.resolve(__dirname,'../Public2/unpaidusers.html'));
  });
  app.get('/NewRegistration',auth1,function(req,res){
    res.sendFile(path.resolve(__dirname,'../Public2/Createprofile.html'));
  });
  app.get('/viewpro_admin',auth1,function(req,res){
    res.sendFile(path.resolve(__dirname,'../Public2/viewprofilebyadmin.html'));
  });
  app.get('/SiteManage',auth1,function(req,res){
    res.sendFile(path.resolve(__dirname,'../Public2/sitemanage.html'));
  });
  app.get('/viewpro_admin',auth1,function(req,res){
    res.sendFile(path.resolve(__dirname,'../Public2/viewprofilebyadmin.html'));
  });
  app.get('/Admineditprofile',auth1,function(req,res){
  res.sendFile(path.resolve(__dirname,'../Public2/admineditprofile.html'));
});
app.get('/ProfileReg_Admin',auth1,function(req,res){
  res.sendFile(path.resolve(__dirname,'../Public2/adminregister.html'));
});

}
