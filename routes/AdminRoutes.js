var async = require('async');
var hart= require('../hart');
var moment = require('moment');
var MongoClient = require('mongodb').MongoClient;
var url = hart.url;
var session = require('express-session');
var mongoose = require('mongoose');
var multer  = require('multer');
var nodemailer = require('nodemailer');
var ObjectId = require('mongodb').ObjectID;
var nodemailer = require('nodemailer');
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, hart.cmsImgPath)
    },
    filename: function (req, file, cb) {
        cb(null, Date.now()+'-'+file.originalname)
    }
})

var upload = multer({ storage: storage });

module.exports = function(app){

  app.use(session({
  secret: 'Niharinfo',
  resave: true,
  saveUninitialized: true
}));

app.post('/latestprofiles',function(req,res){
    var Query = { "$and": [{"mem_status": "1"}] };
    hart.matrimony_users.find(Query).sort({_id:-1}).limit(25).exec(function(err,doc1){
      if(err){
        console.log(err);
      }else{
        res.json(doc1)
      }
    });
})



app.post('/purchase',function(req,res){
  var obj= ObjectId(req.body.hidden);
  var Rajorpayid = req.body.razorpay_payment_id;
  var Membershipplan = req.body.membershipplan;
  hart.matrimony_users.updateOne({'_id':obj},{$set:{ PaymentStatus: "1",Membershipplan: Membershipplan,PaymentRJId: Rajorpayid }},function(err,doc){
    if(!err){
        return res.redirect('/home');
    }else{
        return res.redirect('/home');
    }
  });
})


app.post('/deletesuccessstories',function(req,res){
          var collection2 = hart.succes_stories;
          var obj= ObjectId(req.body.id);
          collection2.updateOne({'_id':obj},{$set:{ Status: "0" }},function(err,doc){
            if(!err){
              res.json("1");
            }else{
              res.json("0");
            }
          });
});
app.post('/deleteevents',function(req,res){
          var collection2 = hart.events;
          var obj= ObjectId(req.body.id);
          collection2.updateOne({'_id':obj},{$set:{ Status: "0" }},function(err,doc){
            if(!err){
              res.json("1");
            }else{
              res.json("0");
            }
          });
});
app.post('/clearviews',function(req,res){
          var collection2 = hart.employees;
          var obj= ObjectId(req.body.employeeid);
          collection2.updateOne({'_id':obj},{$set:{ NoOfViews: 0 }},function(err,doc){
            if(!err){
              res.json("1");
              updateinviewscollect(req.body.employeeid);
            }else{
              res.json("0");
            }
          });
});
function updateinviewscollect(empid){
  var collectio = hart.empviews;
  // collectio.update({ EmployeeId: empid },{$set:{ Status: "0" }}, function(err) {
  collectio.remove({ EmployeeId: empid }, function(err) {
  if (!err) {
          console.log("ok");
  }
  else {
          console.log("not ok");
  }
});
}




app.post('/photosedit',upload.any(), function(req,res){
  if (req.body.submitiontypetype == "edit") {
    if (req.body.photoval == "1") {
      var query = { photo1 :req.files[0].filename }
    }
    if (req.body.photoval == "2") {
      var query = { photo2 :req.files[0].filename }
    }
    if (req.body.photoval == "3") {
      var query = { photo3 :req.files[0].filename }
    }
    if (req.body.photoval == "4") {
      var query = { photo4 :req.files[0].filename }
    }
  }else {
    var query = {photo1 :req.files[0].filename}
  }
    var id = ObjectId(req.session.UserId);
      hart.matrimony_users.updateOne({ '_id': id },{ $set:query}, function(err, response) {
        if(err){
          console.log(err);
          res.json(0);
        }
        else{
            res.json(1);
        }
      });
})


app.post('/bgupload',upload.any(), function(req,res){
            var docs={};
            var files=req.files.filename;
            var bgImage = hart.background;
            var backgrounds = new bgImage({
             bgimage: files,
             Status : "1",
           });
            backgrounds.save(function (err, results) {
              if(err){
                res.json(0);
              }else{
                res.json(1);
              }
            });
    })



    app.post('/deleteprofile',function(req,res){

        var id = ObjectId(req.body.id);
        hart.matrimony_users.updateOne({ '_id': id },{ $set:{ mem_status: "0"}}, function(err, response) {
          if (!err) {
            res.json("1")
          }else{
            res.json("0")
            console.log(err);
          }
        });
    });
    app.post('/updateviews',function(req,res){
      console.log(req.body);
        var id = ObjectId(req.body.id);
        if (req.body.views == "" || req.body.views == null) {
          var gfgfgf = 0;
        }else {
          var gfgfgf = parseInt(req.body.views);
        }
        var numb = gfgfgf+1
        hart.matrimony_users.updateOne({ 'matrimony_id': req.body.matid },{ $set:{ Age: req.body.age}}, function(err, response) {
          if (!err) {
            hart.matrimony_profile.updateOne({ 'matrimony_id': req.body.matid },{ $set:{ views: numb}}, function(err, response) {
              if (!err) {
                res.json("1")
              }else{
                res.json("0")
                console.log(err);
              }
            });
          }else{
            res.json("0")
            console.log(err);
          }
        });
    });
    app.post('/deletephoto',function(req,res){

        var id = ObjectId(req.session.UserId);
        if (req.body.val == "1") {
          var quer = { photo1: ""}
        }
        if (req.body.val == "2") {
          var quer = { photo2: ""}
        }
        if (req.body.val == "3") {
          var quer = { photo3: ""}
        }
        if (req.body.val == "4") {
          var quer = { photo4 : ""}
        }
        hart.matrimony_users.updateOne({ '_id': id },{ $set:quer}, function(err, response) {
          if (!err) {
            res.json("1")
          }else{
            res.json("0")
          }
        });
    });


    app.post('/paymentstatus',function(req,res){
        var ObjectId = require('mongodb').ObjectID;
        var id = ObjectId(req.body.id);
        hart.matrimony_users.updateOne({ '_id': id },{ $set:{ PaymentStatus : req.body.value }}, function(err, response) {
          if (!err) {
            res.json("1");
            updateoldphotos(req.body.email,req.body.matrimony_id,req.body.uid,req.body.id);
          }else{
            res.json("0")
            console.log(err);
          }
        });
    });


    function updateoldphotos(memail,mid,uid,id){
      var idwe = ObjectId(id);
      hart.photos_table.find({ mem_email : memail }).exec(function(err,doc){
        console.log(doc);
        if (!err) {
          if (doc == "" || undefined) {
            hart.matrimony_users.updateOne({ '_id': idwe },{ $set:{ photo1: "",photo2: "",photo3: "",
            photo4: "",photo5: "", }}, function(err, response) {
            if(!err){
              console.log("photos empty updated");
            }
            else{
              console.log(err);
            }
          });
          }else{
            hart.matrimony_users.updateOne({ '_id': idwe },{ $set:{ photo1: doc[0].photo1,
            photo2: doc[0].photo2,
            photo3: doc[0].photo3,
            photo4: "",
            photo5: "", }}, function(err, response) {
            if(!err){
              console.log("photos updated");
            }
            else{
              console.log(err);
            }
          });
          }
        }else{
          return false;
        }
      })
    }
      app.post('/getsuccessstories',function(req,res){
          var collection2 = hart.succes_stories;
          collection2.find({ Status: "1" }).exec(function(err,doc){
            if(!err){
            //  console.log(doc);
              res.json(doc);
            }else{
              res.json("0");
            }
          });
        });
        app.post('/getevents',function(req,res){
            var collection2 = hart.events;
            collection2.find({ Status: "1" }).exec(function(err,doc){
              if(!err){
              //  console.log(doc);
                res.json(doc);
              }else{
                res.json("0");
              }
            });
          });
      app.post('/updateadminee',function(req,res){
          var collection = hart.admins;
          var obj = ObjectId(req.body.adminid);
          collection.updateOne({ '_id': obj },{ $set:{ Name : req.body.name,Email : req.body.email,Mobile : req.body.mobile,Password : req.body.password }}, function(err, response) {
          if(err){
            console.log(err);
          }
          else{
          res.json(1);
          }
          });
      })
}
