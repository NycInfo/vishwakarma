var async = require('async');
var hart= require('../hart');
var MongoClient = require('mongodb').MongoClient;
var url = hart.url;
var session = require('express-session');
var mongoose = require('mongoose');



module.exports = function(app){

  app.use(session({
  secret: 'Niharinfo',
  resave: true,
  saveUninitialized: true
  }));

  app.post('/getallprofiles',function(req,res){
    //console.log(req.body.status);
      if (req.body.status == "All") {
        //var Query = {"mem_status": "1"};
        var Query = {"mem_status": "1"};
      }
      if (req.body.status == "deleted") {
        var Query = {"mem_status": "0"};
      }
      if (req.body.status == "grooms") {
        var Query = { "$and": [{"gender": "MALE"},{"mem_status": "1"}] };
      }
      if (req.body.status == "brides") {
        var Query = { "$and": [{"gender": "FEMALE"},{"mem_status": "1"}] };
      }
      if (req.body.status == "paid") {
        var Query = { "$and": [{"PaymentStatus": "1"},{"mem_status": "1"}] };
      }
      if (req.body.status == "unpaid") {
        var Query = { "$and": [{"PaymentStatus": "0"},{"mem_status": "1"}] };
      }
      hart.matrimony_users.find(Query).sort({_id:-1}).exec(function(err,doc1){
       if(!err){
         res.json(doc1);
       }else{
         res.json("0")
       }
      });
   })
}
