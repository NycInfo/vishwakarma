var async = require('async');
var hart= require('../hart');
var moment = require('moment');
var MongoClient = require('mongodb').MongoClient;
var url = hart.url;
var session = require('express-session');
var mongoose = require('mongoose');
var multer  = require('multer');
var nodemailer = require('nodemailer');

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, hart.cmsImgPath)
    },
    filename: function (req, file, cb) {
        cb(null, Date.now()+'-'+file.originalname)
    }
})

var upload = multer({ storage: storage });

module.exports = function(app){

  app.use(session({
  secret: 'Niharinfo',
  resave: true,
  saveUninitialized: true
  }));



app.post('/getsubcaste',function(req,res){
  hart.subcaste.find().exec(function(err, data){
         if (!err) {
           res.json(data);
         }else {
           res.json("0");
         }
       });
})

app.post('/getbirthstars',function(req,res){
  hart.birth_star.find().exec(function(err, data){
         if (!err) {
           res.json(data);
         }else {
           res.json("0");
         }
       });
})
app.post('/getcountires',function(req,res){
  hart.country.find().exec(function(err, data){
         if (!err) {
           res.json(data);
         }else {
           res.json("0");
         }
       });
})
app.post('/geteducationtable',function(req,res){
  hart.education_table.find().exec(function(err, data){
         if (!err) {
           res.json(data);
         }else {
           res.json("0");
         }
       });
})

app.post('/getoccupationtable',function(req,res){
  hart.occupation_table.find().exec(function(err, data){
         if (!err) {
           res.json(data);
         }else {
           res.json("0");
         }
       });
})
app.post('/getraasi',function(req,res){
  hart.raasi.find().exec(function(err, data){
         if (!err) {
           res.json(data);
         }else {
           res.json("0");
         }
       });
})
app.post('/getmothertongue',function(req,res){
  hart.mother_tongue.find().exec(function(err, data){
         if (!err) {
           res.json(data);
         }else {
           res.json("0");
         }
       });
})

app.post('/getprofiledetailsfromusers',function(req,res){
    var Query = {"uid": req.body.uid};
    hart.matrimony_users.findOne(Query,function(err,data){
      if(err){
        console.log(err);
      }
      else{
    //    console.log(data);
        res.json(data);
      }
     });
})
app.post('/getsubcastewithid',function(req,res){
    var Query = {"sc_id": req.body.dtt};
    hart.subcaste.findOne(Query,function(err,data){
      if(err){
        console.log(err);
      }
      else{
        res.json(data);
      }
     });
})
app.post('/getstartwithid',function(req,res){
    var Query = {"pk_i_id": req.body.star};
    hart.birth_star.findOne(Query,function(err,data){
      if(err){
        console.log(err);
      }
      else{
        res.json(data);
      }
     });
})
app.post('/getrasitwithid',function(req,res){
  console.log(req.body.rasi);
    var Query = {"pk_i_id": req.body.rasi};
    hart.raasi.findOne(Query,function(err,data){
      if(err){
        console.log(err);
      }
      else{
        res.json(data);
      }
     });
})
app.post('/geteducationtwithid',function(req,res){
    var Query = {"education_table_id": req.body.educ};
    hart.education_table.findOne(Query,function(err,data){
      if(err){
        console.log(err);
      }
      else{
        res.json(data);
      }
     });
})
app.post('/getprofiledetailsfromprofile',function(req,res){
    var Query = {"uid": req.body.uid};
    hart.matrimony_profile.findOne(Query,function(err,data){
      if(err){
        console.log(err);
      }
      else{
    //    console.log(data);
        res.json(data);
      }
     });
})






app.post('/getmyprofiledetailsfromusers',function(req,res){
  if (req.body.source=='admin') {
  var Query = {"uid": req.body.uid};
  }else {
    var Query = {"uid": req.session.uid};
  }
    hart.matrimony_users.findOne(Query,function(err,data){
      if(err){
        console.log(err);
      }
      else{
    //    console.log(data);
        res.json(data);
      }
     });
})

app.post('/getmyprofiledetailsfromprofile',function(req,res){
  if (req.body.source=='admin') {
  var Query = {"uid": req.body.uid};
  }else {
    var Query = {"uid": req.session.uid};
  }
    hart.matrimony_profile.findOne(Query,function(err,data){
      if(err){
        console.log(err);
      }
      else{
        res.json(data);
      }
     });
})




app.post('/Newgetprofiles',function(req,res){
  if(req.session.gender == "MALE"){
    var Gender = "FEMALE";
    var Query = { "$and": [{"gender": Gender},{"mem_status": "1"}] };

  }else{
    var Gender = "MALE";
    var Query = { "$and": [{"gender": Gender},{"mem_status": "1"}] };
  }
  if(req.body.pagination != ""){
    var skipby = parseInt(req.body.pagination);
  }else{
    var skipby = 0;
  }
  hart.matrimony_users.find(Query).sort({_id:-1}).skip(skipby).limit(25).exec(function(err,doc1){
    if(err){
      console.log(err);
    }else{
      res.json(doc1)
    }
  });
})

  app.post('/getprofiles',function(req,res){
    if(req.session.gender == "MALE"){
      var Gender = "FEMALE";
      var Query = { "$and": [{"gender": Gender},{"mem_status": "1"}] };

    }else{
      var Gender = "MALE";
      var Query = { "$and": [{"gender": Gender},{"mem_status": "1"}] };
    }
    if(req.body.pagination != ""){
      var skipby = parseInt(req.body.pagination)*25;
    }else{
      var skipby = 0;
    }

    hart.matrimony_users.find(Query).sort({_id:-1}).skip(skipby).limit(25).exec(function(err,doc1){
      if(err){
        console.log(err);
      }else{
        res.json(doc1)
      }
    });
  })
  app.post('/getprofileslength',function(req,res){
    if(req.session.gender == "MALE"){
      var Gender = "FEMALE";
      var Query = { "$and": [{"gender": Gender},{"mem_status": "1"}] };

    }else{
      var Gender = "MALE";
      var Query = { "$and": [{"gender": Gender},{"mem_status": "1"}] };
    }
    if(req.body.pagination != ""){
      var skipby = parseInt(req.body.pagination);
    }else{
      var skipby = 0;
    }
    hart.matrimony_users.find(Query).exec(function(err,doc1){
      if(err){
        console.log(err);
      }else{
        res.json(doc1)
      }
    });
  })
  app.post('/getsimilarprofiles',function(req,res){
    if(req.session.gender == "MALE"){
      var Gender = "FEMALE";
      var Query = { "$and": [{"gender": Gender},{"mem_status": "1"}] };

    }else{
      var Gender = "MALE";
      var Query = { "$and": [{"gender": Gender},{"mem_status": "1"}] };
    }
    if(req.body.pagination != ""){
      var skipby = parseInt(req.body.pagination);
    }else{
      var skipby = 0;
    }
    hart.matrimony_users.find(Query).sort({_id:-1}).skip(skipby).limit(10).exec(function(err,doc1){
      if(err){
        console.log(err);
      }else{
        res.json(doc1)
      }
    });
  })


  app.post('/SpecialSearch',function(req,res){
        if (req.session.gender == "MALE") {
          var gender = "FEMALE";
        }else {
          var gender = "MALE";
        }
        //{"Age":{$gt:req.body.agefrom}},{"Age":{$lt:req.body.ageto}}
       if (req.body.memberid != "") {
         hart.matrimony_users.find({ "$and": [{"mem_status":"1"},{ "$and": [{"matrimony_id": req.body.memberid}] }] }).exec(function(err,doc){
           if(!err){
             res.json(doc);
           }else{
             res.json("0");
           }
         });
         return false;
       }else {
         if(req.body.pagination != ""){
           var skipby = parseInt(req.body.pagination)*25;
         }else{
           var skipby = 0;
         }

         if (req.body.maritalstatus == "ALL") {
           if (req.body.caste != "ALL") {
             hart.matrimony_users.find({ "$and": [{"gender":gender},{"mem_status":"1"},{"sub_cast":req.body.caste},{"Age":{$gt:req.body.agefrom}},{"Age":{$lt:req.body.ageto}}] }).skip(skipby).limit(20).exec(function(err,doc){
               if(!err){
                 res.json(doc);
               }else{
                 res.json("0");
               }
             });
             return false;
           }else {
             hart.matrimony_users.find({ "$and": [{"gender":gender},{"mem_status":"1"},{"Age":{$gt:req.body.agefrom}},{"Age":{$lt:req.body.ageto}}] }).skip(skipby).limit(20).exec(function(err,doc){
               if(!err){
                 res.json(doc);
               }else{
                 res.json("0");
               }
             });
             return false;
           }
         }
         if (req.body.caste == "ALL") {
           if (req.body.maritalstatus != "ALL") {
             hart.matrimony_users.find({ "$and": [{"gender":gender},{"mem_status":"1"},{"marital_status":req.body.maritalstatus},{"Age":{$gt:req.body.agefrom}},{"Age":{$lt:req.body.ageto}}] }).skip(skipby).limit(20).exec(function(err,doc){
               if(!err){
                 res.json(doc);
               }else{
                 res.json("0");
               }
             });
             return false;
           }else {
             hart.matrimony_users.find({ "$and": [{"gender":gender},{"mem_status":"1"},{"Age":{$gt:req.body.agefrom}},{"Age":{$lt:req.body.ageto}}] }).skip(skipby).limit(20).exec(function(err,doc){
               if(!err){
                 res.json(doc);
               }else{
                 res.json("0");
               }
             });
             return false;
           }
         }
         else{
           hart.matrimony_users.find({ "$and": [{"gender":gender},{"mem_status":"1"},{"marital_status": req.body.maritalstatus},{"sub_cast":req.body.caste},{"Age":{$gt:req.body.agefrom}},{"Age":{$lt:req.body.ageto}}] }).skip(skipby).limit(20).exec(function(err,doc){
             if(!err){
               res.json(doc);
             }else{
               res.json("0");
             }
           });
           return false;
         }
       }

   });



   app.post('/getprofileslength_search',function(req,res){
     if (req.session.gender == "MALE") {
       var gender = "FEMALE";
     }else {
       var gender = "MALE";
     }
     if (req.body.maritalstatus == "ALL") {
       if (req.body.caste != "ALL") {
         hart.matrimony_users.find({ "$and": [{"gender":gender},{"mem_status":"1"},{"sub_cast":req.body.caste},{"Age":{$gt:req.body.agefrom}},{"Age":{$lt:req.body.ageto}}] }).exec(function(err,doc){
           if(!err){
             res.json(doc);
           }else{
             res.json("0");
           }
         });
         return false;
       }else {
         hart.matrimony_users.find({ "$and": [{"gender":gender},{"mem_status":"1"},{"Age":{$gt:req.body.agefrom}},{"Age":{$lt:req.body.ageto}}] }).exec(function(err,doc){
           if(!err){
             res.json(doc);
           }else{
             res.json("0");
           }
         });
         return false;
       }
     }
     if (req.body.caste == "ALL") {
       if (req.body.maritalstatus != "ALL") {
         hart.matrimony_users.find({ "$and": [{"gender":gender},{"mem_status":"1"},{"marital_status":req.body.maritalstatus},{"Age":{$gt:req.body.agefrom}},{"Age":{$lt:req.body.ageto}}] }).exec(function(err,doc){
           if(!err){
             res.json(doc);
           }else{
             res.json("0");
           }
         });
         return false;
       }else {
         hart.matrimony_users.find({ "$and": [{"gender":gender},{"mem_status":"1"},{"Age":{$gt:req.body.agefrom}},{"Age":{$lt:req.body.ageto}}] }).exec(function(err,doc){
           if(!err){
             res.json(doc);
           }else{
             res.json("0");
           }
         });
         return false;
       }
     }
     else{
       hart.matrimony_users.find({ "$and": [{"gender":gender},{"mem_status":"1"},{"marital_status": req.body.maritalstatus},{"sub_cast":req.body.caste},{"Age":{$gt:req.body.agefrom}},{"Age":{$lt:req.body.ageto}}] }).exec(function(err,doc){
         if(!err){
           res.json(doc);
         }else{
           res.json("0");
         }
       });
       return false;
     }
   })

}
